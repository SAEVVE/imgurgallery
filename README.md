Hello everyone! This is Android project. The main goal of it - loads and 
shows images from Imgur service.

In this project i use the next stack:

1. Kotlin
2. RxJava 2
3. Retorfit 2
4. Room database
5. MVP as main architecure approach
6. Dagger 2
7. Mockito for unit tests
8. Picasso
