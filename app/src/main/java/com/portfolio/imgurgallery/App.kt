package com.portfolio.imgurgallery

import android.app.Application
import com.portfolio.imgurgallery.di.*

class App : Application() {
    companion object {
        @JvmStatic
        lateinit var appComponent: AppComponent
    }

    override fun onCreate() {
        super.onCreate()

        appComponent = DaggerAppComponent.builder()
            .apiModule(ApiModule())
            .repositoriesModule(RepositoriesModule())
            .databaseModule(DatabaseModule())
            .presenterModule(PresenterModule())
            .schedulersModule(SchedulersModule())
            .storageModule(StorageModule())
            .contextModule(ContextModule(applicationContext))
            .build()
    }
}
