package com.portfolio.imgurgallery

import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.jakewharton.rxbinding3.swiperefreshlayout.refreshes
import com.jakewharton.rxbinding3.widget.checkedChanges
import com.portfolio.imgurgallery.gallery.MessageType
import com.portfolio.imgurgallery.recyclerview.GridSpacingItemDecoration
import com.portfolio.imgurgallery.recyclerview.RecyclerViewAdapter
import com.portfolio.imgurgallery.util.AppError
import com.portfolio.imgurgallery.util.ErrorType
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.Observables
import io.reactivex.subjects.BehaviorSubject
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*

class MainActivity : AppCompatActivity() {

    companion object {
        private const val COLUMN_COUNT = 2
    }

    private val subscription = CompositeDisposable()
    private val pullToRefreshSubject = BehaviorSubject.createDefault(Unit)
    private val reloadFeedAfterErrorSubject = BehaviorSubject.createDefault(Unit)

    private val tags = listOf("car", "skis")

    private var errorMessageSnackbar: Snackbar? = null

    private lateinit var adapter: RecyclerViewAdapter
    private val galleryPresenter = App.appComponent.getGalleryPresenter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        adapter = RecyclerViewAdapter(this, App.appComponent.getRuntimeSchedulers())
        postsRecyclerView.adapter = adapter
        val gridLayoutManager = GridLayoutManager(this, COLUMN_COUNT)
        gridLayoutManager.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
            override fun getSpanSize(position: Int): Int {
                val type = adapter.getItemViewType(position)
                if (type == RecyclerViewAdapter.FULL_ITEM_TYPE || type == RecyclerViewAdapter.EMPTY_FULL_ITEM_TYPE) {
                    return 2
                }
                return 1
            }
        }
        postsRecyclerView.layoutManager = gridLayoutManager
        postsRecyclerView.addItemDecoration(
            GridSpacingItemDecoration(
                COLUMN_COUNT,
                resources.getDimension(R.dimen.side_bottom_margin).toInt()
            )
        )

        subscription.addAll(
            galleryPresenter.onErrorOccurs()
                .subscribe {
                    showErrorMessage(it)
                },

            galleryPresenter.onErrorHides()
                .subscribe {
                    errorMessageSnackbar?.dismiss()
                },

            galleryPresenter.onDataLoading().subscribe {
                postLoadingProgress.visibility = if (it) View.VISIBLE else View.INVISIBLE
            },

            pullToRefresh.refreshes().subscribe {
                pullToRefreshSubject.onNext(Unit)
            },

            galleryPresenter.onShowMessage()
                .subscribe {
                    val message = when (it) {
                        MessageType.NO_DATA -> resources.getString(R.string.no_content)
                        MessageType.DATA_UPDATED -> resources.getString(R.string.data_updated)
                        MessageType.ITEM_DELETED -> resources.getString(R.string.data_deleted)
                        else -> ""
                    }
                    if (message.isNotEmpty()) {
                        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
                    }
                },

            galleryPresenter.onDataCome()
                .doOnNext {
                    pullToRefresh.isRefreshing = false
                    setUpdateEnabled(true)
                }
                .subscribe {
                    adapter.resultsWithBigCards(bigPostSwitcher.isChecked)
                    adapter.setData(it)
                },

            adapter.onPostDeleteClicked().subscribe {
                galleryPresenter.deletePost(it)
            },

            Observables.combineLatest(reloadFeedAfterErrorSubject, pullToRefreshSubject, bigPostSwitcher.checkedChanges())
                .doOnNext {
                    setUpdateEnabled(false)
                }
                .observeOn(AndroidSchedulers.mainThread())
                .map { it.third }
                .subscribe {
                    if (it) {
                        galleryPresenter.loadData(tags)
                    } else {
                        galleryPresenter.loadData(listOf(tags[0]))
                    }
                },

            galleryPresenter.onPostDeleted().subscribe {
                adapter.deletePostFromFeed(it)
            }
        )
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun showErrorMessage(error: AppError) {
        errorMessageSnackbar = Snackbar.make(mainContent, getMessageForErrorType(error.errorType), Snackbar.LENGTH_LONG)

        errorMessageSnackbar?.let { snackbar: Snackbar ->
            if (error.isRetryable) {
                snackbar.setAction(resources.getString(R.string.retry)) {
                    reloadFeedAfterErrorSubject.onNext(Unit)
                    snackbar.dismiss()
                }
            }
            snackbar.show()
        }
    }

    private fun getMessageForErrorType(errorType: ErrorType): String {
        return when (errorType) {
            ErrorType.ACCESS_ERROR -> resources.getString(R.string.access_error)
            ErrorType.AUTHENTICATION_ERROR -> resources.getString(R.string.authentication_error)
            ErrorType.CONNECTION_TIMEOUT_ERROR -> resources.getString(R.string.connection_timeout_error)
            ErrorType.INTERNET_ABSENCE_ERROR -> resources.getString(R.string.internet_error)
            ErrorType.RATE_LIMIT_ERROR -> resources.getString(R.string.rate_limit_error)
            ErrorType.SERVER_ERROR -> resources.getString(R.string.server_error)
            ErrorType.UNKNOWN_ERROR -> resources.getString(R.string.unknown_error)
            ErrorType.DATABASE_ERROR -> resources.getString(R.string.database_error)
        }
    }

    private fun setUpdateEnabled(isEnabled: Boolean) {
        bigPostSwitcher.isEnabled = isEnabled
        pullToRefresh.isEnabled = isEnabled
    }

    override fun onDestroy() {
        super.onDestroy()
        subscription.clear()
        galleryPresenter.onViewDestroying()
        adapter.onViewDestroying()
    }
}
