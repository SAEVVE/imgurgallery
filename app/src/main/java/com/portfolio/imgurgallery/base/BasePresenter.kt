package com.portfolio.imgurgallery.base

import com.portfolio.imgurgallery.util.AppError
import com.portfolio.imgurgallery.util.ErrorHandler
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.PublishSubject

abstract class BasePresenter(private val repository: BaseRepository,
                             private val errorHandler: ErrorHandler) {
    private val errorOccursSubject = PublishSubject.create<AppError>()
    private val errorHidesSubject = PublishSubject.create<Unit>()

    protected var subscriptions = CompositeDisposable()

    fun onErrorOccurs() : Observable<AppError> = errorOccursSubject
    fun onErrorHides(): Observable<Unit> = errorHidesSubject

    protected fun errorOccurred(it: Throwable) {
        val appError = errorHandler.getAppErrorByThrowable(it)
        errorOccursSubject.onNext(appError)
    }

    protected fun hideError() {
        errorHidesSubject.onNext(Unit)
    }

    fun onViewDestroying() {
        subscriptions.clear()
        repository.onPresenterDestroying()
    }
}