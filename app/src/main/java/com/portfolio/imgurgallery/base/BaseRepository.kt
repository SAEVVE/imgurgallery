package com.portfolio.imgurgallery.base

import io.reactivex.disposables.CompositeDisposable

open class BaseRepository {
    protected var subscriptions = CompositeDisposable()

    fun onPresenterDestroying() {
        subscriptions.clear()
    }
}