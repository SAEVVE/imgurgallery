package com.portfolio.imgurgallery.dao

import androidx.room.*
import com.portfolio.imgurgallery.dto.ImgurPost
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.Single

@Dao
interface ImgurDao {
    @Insert
    fun insert(posts: List<ImgurPost>): List<Long>

    @Query("SELECT * FROM ImgurPost WHERE primaryKeyId IN (:ids)")
    fun getPosts(ids: List<Long>) : Single<List<ImgurPost>>

    @Query("DELETE FROM ImgurPost")
    fun deleteAll()

    @Delete
    fun deletePost(it: ImgurPost): Completable
}