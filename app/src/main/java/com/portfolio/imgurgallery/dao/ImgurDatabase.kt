package com.portfolio.imgurgallery.dao

import android.content.Context
import androidx.room.*
import com.portfolio.imgurgallery.dto.ImgurPost

@Database(entities = [ImgurPost::class], version = 1, exportSchema = false)
@TypeConverters(ImgurImageTypeConverter::class)
abstract class ImgurDatabase: RoomDatabase() {
    abstract fun imgurDao(): ImgurDao

    companion object {
        private const val DATABASE_NAME = "ImgurPosts.db"
        @Volatile private var INSTANCE: ImgurDatabase? = null

        fun getInstance(context: Context): ImgurDatabase =
            INSTANCE ?: synchronized(this) {
                INSTANCE ?: buildDatabase(context).also { INSTANCE = it }
            }

        private fun buildDatabase(context: Context) =
            Room.databaseBuilder(context.applicationContext,
                ImgurDatabase::class.java, DATABASE_NAME)
                .build()
    }
}