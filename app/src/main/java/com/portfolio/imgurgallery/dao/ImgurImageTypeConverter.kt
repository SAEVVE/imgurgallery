package com.portfolio.imgurgallery.dao

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.portfolio.imgurgallery.dto.Image

object ImgurImageTypeConverter {

    @TypeConverter
    @JvmStatic
    fun stringToList(data: String): List<Image> {
        val gson = Gson()
        val type = object : TypeToken<List<Image>>() {}.type
        return gson.fromJson(data, type)
    }

    @TypeConverter
    @JvmStatic
    fun listToString(list: List<Image>): String {
        val gson = Gson()
        return gson.toJson(list)
    }

}