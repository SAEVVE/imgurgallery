package com.portfolio.imgurgallery.dao

import com.portfolio.imgurgallery.dto.ImgurPost
import io.reactivex.Completable
import io.reactivex.Single

abstract class ImgurPostStorage {
    abstract fun deletePost(post: ImgurPost): Completable
    abstract fun getDataByIds(ids: List<Long>): Single<List<ImgurPost>>
    abstract fun insert(posts: List<ImgurPost>): Single<List<Long>>
}