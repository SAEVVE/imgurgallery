package com.portfolio.imgurgallery.dao

import com.portfolio.imgurgallery.dto.ImgurPost
import io.reactivex.Completable
import io.reactivex.Single

open class ImgurPostStorageRoom(private val imgurDatabase: ImgurDatabase) : ImgurPostStorage() {
    override fun insert(posts: List<ImgurPost>): Single<List<Long>> {
        return Single.create {
            imgurDatabase.runInTransaction {
                try {
                    imgurDatabase.imgurDao().deleteAll()
                    imgurDatabase.imgurDao().insert(posts)
                    it.onSuccess(imgurDatabase.imgurDao().insert(posts))
                } catch (e: Exception) {
                    it.onError(e)
                }
            }
        }
    }

    override fun getDataByIds(ids: List<Long>): Single<List<ImgurPost>> {
        return imgurDatabase.imgurDao().getPosts(ids)
    }

    override fun deletePost(post: ImgurPost): Completable {
        return imgurDatabase.imgurDao().deletePost(post)
    }
}