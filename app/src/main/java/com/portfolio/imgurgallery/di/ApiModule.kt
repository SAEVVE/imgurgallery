package com.portfolio.imgurgallery.di

import com.portfolio.imgurgallery.gallery.GalleryApi
import dagger.Module
import dagger.Provides
import okhttp3.Headers
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
@Singleton
class ApiModule {
    private val TIMEOUT = 25L
    @Provides
    fun provideOkHttpClient(): OkHttpClient {
        val client = OkHttpClient().newBuilder()
        val headers = mutableMapOf<String, String>()

        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY
        client.addInterceptor(logging)

        client.connectTimeout(TIMEOUT, TimeUnit.SECONDS)
        client.readTimeout(TIMEOUT, TimeUnit.SECONDS)

        headers["Accept"] = "application/json"
        headers["Content-Type"] = "application/json"
        headers["Authorization"] = "Client-ID 469f82a9c651414"

        client.addInterceptor { chain ->
            val original = chain.request()
            val builder = original.newBuilder()
                .headers(Headers.of(headers))
                .method(original.method(), original.body())
            val request = builder.build()
            chain.proceed(request)
        }
        return client.build()
    }

    @Provides
    fun provideCurrencyServiceJson(client: OkHttpClient): GalleryApi {
        val retrofitBuilder = Retrofit.Builder()
                .baseUrl("https://api.imgur.com/3/")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(client)
        return retrofitBuilder.build().create(GalleryApi::class.java)
    }
}
