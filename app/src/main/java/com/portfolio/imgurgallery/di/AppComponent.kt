package com.portfolio.imgurgallery.di

import com.portfolio.imgurgallery.gallery.GalleryPresenter
import com.portfolio.imgurgallery.util.schedulers.RuntimeSchedulers
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [ApiModule::class,
    RepositoriesModule::class,
    PresenterModule::class,
    ContextModule::class,
    StorageModule::class,
    DatabaseModule::class,
    SchedulersModule::class,
    ErrorHandlerModule::class]
)

interface AppComponent {
    fun getGalleryPresenter(): GalleryPresenter
    fun getRuntimeSchedulers(): RuntimeSchedulers
}
