package com.portfolio.imgurgallery.di

import android.content.Context
import com.portfolio.imgurgallery.dao.ImgurDatabase
import dagger.Module
import dagger.Provides

@Module
class DatabaseModule {
    @Provides
    fun provideDatabase(context: Context): ImgurDatabase {
        return ImgurDatabase.getInstance(context)
    }
}