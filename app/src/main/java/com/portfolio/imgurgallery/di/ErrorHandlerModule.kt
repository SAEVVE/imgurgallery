package com.portfolio.imgurgallery.di

import com.portfolio.imgurgallery.util.ErrorHandler
import dagger.Module
import dagger.Provides

@Module
class ErrorHandlerModule {
    @Provides
    fun provideErrorHandler(): ErrorHandler {
        return ErrorHandler()
    }
}