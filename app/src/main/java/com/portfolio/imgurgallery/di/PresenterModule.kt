package com.portfolio.imgurgallery.di

import com.portfolio.imgurgallery.gallery.GalleryPresenter
import com.portfolio.imgurgallery.gallery.GalleryRepository
import com.portfolio.imgurgallery.util.ErrorHandler
import com.portfolio.imgurgallery.util.schedulers.RuntimeSchedulers
import dagger.Module
import dagger.Provides

@Module
class PresenterModule {
    @Provides
    fun provideGalleryPresenter(repository: GalleryRepository, schedulers: RuntimeSchedulers,
                                errorHandler: ErrorHandler) =
        GalleryPresenter(repository, schedulers, errorHandler)
}