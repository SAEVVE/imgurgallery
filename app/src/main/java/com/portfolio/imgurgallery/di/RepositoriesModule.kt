package com.portfolio.imgurgallery.di

import com.portfolio.imgurgallery.gallery.GalleryApi
import com.portfolio.imgurgallery.dao.ImgurPostStorage
import com.portfolio.imgurgallery.gallery.GalleryRepository
import com.portfolio.imgurgallery.util.schedulers.RuntimeSchedulers
import dagger.Module
import dagger.Provides

@Module
class RepositoriesModule {
    @Provides
    fun provideGalleryRepository(galleryService: GalleryApi, storage: ImgurPostStorage, schedulers: RuntimeSchedulers): GalleryRepository {
        return GalleryRepository(galleryService, storage, schedulers)
    }
}
