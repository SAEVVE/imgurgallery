package com.portfolio.imgurgallery.di

import com.portfolio.imgurgallery.util.schedulers.RuntimeSchedulers
import com.portfolio.imgurgallery.util.schedulers.TestSchedulers
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class SchedulersModule {
    @Singleton
    @Provides
    fun provideRuntimeSchedulers() : RuntimeSchedulers {
        return RuntimeSchedulers()
    }

    @Singleton
    @Provides
    fun provideTestSchedulers(): TestSchedulers {
        return TestSchedulers()
    }
}