package com.portfolio.imgurgallery.di

import com.portfolio.imgurgallery.dao.ImgurDatabase
import com.portfolio.imgurgallery.dao.ImgurPostStorage
import com.portfolio.imgurgallery.dao.ImgurPostStorageRoom
import dagger.Module
import dagger.Provides

@Module
class StorageModule {
    @Provides
    fun provideImgurRoomStorage(imgurDatabase: ImgurDatabase) : ImgurPostStorage {
        return ImgurPostStorageRoom(imgurDatabase)
    }
}