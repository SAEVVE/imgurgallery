package com.portfolio.imgurgallery.dto

import androidx.room.Entity
import androidx.room.PrimaryKey

data class ImgurWholeAnswer(val data: ImgurData)

data class ImgurData(val items: List<ImgurPost>?)

@Entity
data class ImgurPost(
    @PrimaryKey(autoGenerate = true)
    var primaryKeyId: Long,
    val id: String,
    val title: String,
    val datetime: Long,
    val views: Long,
    val scores: Long,
    val images: List<Image>?,
    var postType: Int
)

data class Image(val link: String, val type: String, val animated: Boolean)