package com.portfolio.imgurgallery.gallery

import com.portfolio.imgurgallery.dto.ImgurWholeAnswer
import io.reactivex.Flowable
import io.reactivex.Maybe
import retrofit2.http.GET
import retrofit2.http.Path

interface GalleryApi {
    @GET("gallery/t/{tagName}")
    fun getImagesByTag(@Path("tagName") tagName: String) : Flowable<ImgurWholeAnswer>
}
