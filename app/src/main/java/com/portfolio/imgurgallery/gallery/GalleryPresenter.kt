package com.portfolio.imgurgallery.gallery

import com.portfolio.imgurgallery.base.BasePresenter
import com.portfolio.imgurgallery.dto.ImgurPost
import com.portfolio.imgurgallery.util.ErrorHandler
import com.portfolio.imgurgallery.util.schedulers.BaseSchedulers
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

class GalleryPresenter(private val repository: GalleryRepository,
                       private val schedulers: BaseSchedulers,
                       errorHandler: ErrorHandler)
    : BasePresenter(repository, errorHandler) {

    private val dataSubject = PublishSubject.create<List<ImgurPost>>()
    private val loadDataSubject = PublishSubject.create<List<String>>()
    private val onPostDeletedSubject = PublishSubject.create<ImgurPost>()
    private val messagesSubject = PublishSubject.create<MessageType>()
    private val deletePostSubject = PublishSubject.create<ImgurPost>()

    private val dataLoadingProgressSubject = PublishSubject.create<Boolean>()

    init {
        subscriptions.addAll(
            loadDataSubject
                .doOnNext { dataLoadingProgressSubject.onNext(true) }
                .flatMapSingle {
                    repository.loadImagesByTag(it)
                }
                .observeOn(schedulers.ui())
                .doOnError {
                    dataSubject.onNext(emptyList())
                    hideError()
                    errorOccurred(it)
                    dataLoadingProgressSubject.onNext(false)
                }
                .retry()
                .subscribe {
                    hideError()
                    if (it.isEmpty()) {
                        messagesSubject.onNext(MessageType.NO_DATA)
                    } else {
                        messagesSubject.onNext(MessageType.DATA_UPDATED)
                    }
                    dataLoadingProgressSubject.onNext(false)
                    dataSubject.onNext(it)
                },

            deletePostSubject.flatMapCompletable {
                repository.deletePost(it)
                    .observeOn(schedulers.ui())
                    .doOnComplete {
                        messagesSubject.onNext(MessageType.ITEM_DELETED)
                        onPostDeletedSubject.onNext(it)
                    }
            }.doOnError {
                errorOccurred(it)
            }
                .retry()
                .subscribe()
        )
    }

    fun onDataCome(): Observable<List<ImgurPost>> = dataSubject

    fun loadData(tags: List<String>) = loadDataSubject.onNext(tags)

    fun deletePost(post: ImgurPost) = deletePostSubject.onNext(post)

    fun onPostDeleted(): Observable<ImgurPost> = onPostDeletedSubject

    fun onShowMessage(): Observable<MessageType> = messagesSubject

    fun onDataLoading(): Observable<Boolean> = dataLoadingProgressSubject

}

enum class MessageType {
    NO_DATA,
    DATA_UPDATED,
    ITEM_DELETED
}