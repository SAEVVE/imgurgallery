package com.portfolio.imgurgallery.gallery

import com.portfolio.imgurgallery.base.BaseRepository
import com.portfolio.imgurgallery.dao.ImgurPostStorage
import com.portfolio.imgurgallery.dto.ImgurPost
import com.portfolio.imgurgallery.dto.ImgurWholeAnswer
import com.portfolio.imgurgallery.util.schedulers.BaseSchedulers
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import java.util.*

open class GalleryRepository(private val api: GalleryApi, private val storage: ImgurPostStorage, private val schedulers: BaseSchedulers) : BaseRepository() {

     open fun deletePost(post: ImgurPost): Completable {
        return storage.deletePost(post)
            .subscribeOn(schedulers.io())
    }

    open fun loadImagesByTag(tags: List<String>): Single<List<ImgurPost>> {
        return Flowable.zip(getFlowablesFromIncomingTags(tags))
        { answers -> getFeedFromApiAnswer(answers.filter { it is ImgurWholeAnswer } as List<ImgurWholeAnswer>) }
            .flatMapSingle { storage.insert(it).flatMap { storage.getDataByIds(it) } }
            .singleOrError()
            .subscribeOn(schedulers.io())
    }

    private fun getFlowablesFromIncomingTags(tags: List<String>): List<Flowable<ImgurWholeAnswer>> {
        return tags.map { api.getImagesByTag(it) }
    }

    private fun getFeedFromApiAnswer(answers: List<ImgurWholeAnswer>): List<ImgurPost> {
        val itemsList = LinkedList<ImgurPost>()
        answers.forEach {
            it.data.items?.run {
                itemsList.addAll(this)
            }
        }
        return filterPostForValidImages(itemsList)
    }

    private fun filterPostForValidImages(posts: List<ImgurPost>): List<ImgurPost> {
        if (posts.isEmpty()) return emptyList()
        val validPosts = LinkedList<ImgurPost>()
        posts.forEach {
            it.images?.filterNot { it.animated }?.run {
                if (isNotEmpty()) {
                    validPosts.add(it.copy(images = this))
                }
            }
        }
        return validPosts
    }
}
