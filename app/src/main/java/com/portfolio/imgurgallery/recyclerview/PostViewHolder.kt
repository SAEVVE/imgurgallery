package com.portfolio.imgurgallery.recyclerview

import android.view.View
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.portfolio.imgurgallery.R
import com.portfolio.imgurgallery.dto.ImgurPost
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.adt_post_mini.view.*

class PostViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    fun bindView(post: ImgurPost) {
        if (post.images == null || post.images.isEmpty()) return
        downloadImage(post.images[0].link, itemView.postImage)
    }

    private fun downloadImage(url: String, imageView: ImageView) {
        Picasso.get()
            .load(url)
            .fit()
            .centerCrop()
            .placeholder(R.drawable.ic_photo_black_100dp)
            .error(R.drawable.ic_sentiment_very_dissatisfied_black_100dp)
            .into(imageView)
    }
}