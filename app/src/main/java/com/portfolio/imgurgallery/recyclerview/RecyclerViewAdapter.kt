package com.portfolio.imgurgallery.recyclerview

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.jakewharton.rxbinding3.view.clicks
import com.portfolio.imgurgallery.R
import com.portfolio.imgurgallery.dto.ImgurPost
import com.portfolio.imgurgallery.util.Animation
import com.portfolio.imgurgallery.util.schedulers.BaseSchedulers
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.adt_post_full.view.*
import java.lang.IllegalArgumentException
import java.util.LinkedList
import java.util.concurrent.TimeUnit

class RecyclerViewAdapter(val context: Context, private val schedulers: BaseSchedulers) : RecyclerView.Adapter<PostViewHolder>() {
    private val subscriptions = CompositeDisposable()
    private val data = arrayListOf<ImgurPost>()
    private val deleteBigPostClickedSubject = PublishSubject.create<ImgurPost>()
    private var resultsWithBigCards = false
    private var deleteButtonAnimation : Animator? = null

    companion object {
        private const val DELETE_ACTION_PAUSE = 500L
        private const val BIG_POST_ORDER_POSITION = 7
        private const val DUMMY_DATA_SIZE = 10
        internal const val MINI_ITEM_TYPE = 0
        internal const val FULL_ITEM_TYPE = 1
        internal const val EMPTY_MINI_ITEM_TYPE = 2
        internal const val EMPTY_FULL_ITEM_TYPE = 3
    }

    fun deletePostFromFeed(post: ImgurPost) {
        val position = data.indexOf(post)
        data.removeAt(position)
        notifyItemRemoved(position)
        notifyItemRangeChanged(position, data.size);
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostViewHolder {
        val view: View = when(viewType) {
            EMPTY_MINI_ITEM_TYPE, MINI_ITEM_TYPE -> {
                LayoutInflater.from(context).inflate(R.layout.adt_post_mini, parent, false)
            }
            EMPTY_FULL_ITEM_TYPE, FULL_ITEM_TYPE -> {
                LayoutInflater.from(context).inflate(R.layout.adt_post_full, parent, false)
            }
            else -> LayoutInflater.from(context).inflate(R.layout.adt_post_mini, parent, false)
        }
        return PostViewHolder(view)
    }

    override fun getItemViewType(position: Int): Int {
        return data[position].postType
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: PostViewHolder, position: Int) {
        holder.bindView(data[position])
        if (getItemViewType(position) == FULL_ITEM_TYPE) {
            holder.itemView.deleteBigPostButton.visibility = View.VISIBLE
            subscriptions.add(
                holder.itemView.deleteBigPostButton.clicks()
                    .throttleFirst(DELETE_ACTION_PAUSE, TimeUnit.MILLISECONDS)
                    .observeOn(schedulers.ui())
                    .subscribe {
                    deleteButtonAnimation = Animation.getScaleAnimation(holder.itemView.deleteBigPostButton)
                    deleteButtonAnimation!!.addListener(object: AnimatorListenerAdapter() {
                        override fun onAnimationEnd(animation: Animator?) {
                            deleteBigPostClickedSubject.onNext(data[position])
                        }
                    })
                    deleteButtonAnimation!!.start()
                }
            )
        }
    }

    fun setData(posts: List<ImgurPost>) {
        data.clear()
        if (posts.isNotEmpty()) {
            data.addAll(posts)
            data.forEach {
                it.postType = getPostTypeByPosition(data.indexOf(it))
            }
        } else {
            data.addAll(createDummyDataList())
        }
        notifyDataSetChanged()
    }

    fun onPostDeleteClicked(): Observable<ImgurPost> = deleteBigPostClickedSubject

    private fun createDummyDataList(): List<ImgurPost> {
        val placeHolderItems = LinkedList<ImgurPost>()
        for (i in 0 until DUMMY_DATA_SIZE) {
            placeHolderItems.add(ImgurPost(0, "", "", 0, 0, 0,
                emptyList(),
                convertToEmptyItemType(getPostTypeByPosition(i))))
        }
        return placeHolderItems
    }

    fun resultsWithBigCards(it: Boolean) {
        resultsWithBigCards = it
    }

    private fun getPostTypeByPosition(position: Int): Int {
        if (!resultsWithBigCards) return MINI_ITEM_TYPE
        if (position < BIG_POST_ORDER_POSITION - 1) return MINI_ITEM_TYPE
        if ((position + 1) % BIG_POST_ORDER_POSITION == 0) return FULL_ITEM_TYPE
        return MINI_ITEM_TYPE
    }

    private fun convertToEmptyItemType(itemType: Int) : Int {
        return when(itemType) {
            MINI_ITEM_TYPE -> EMPTY_MINI_ITEM_TYPE
            FULL_ITEM_TYPE -> EMPTY_FULL_ITEM_TYPE
            else -> throw IllegalArgumentException("Invalid Item's type!")
        }
    }

    fun onViewDestroying() {
        deleteButtonAnimation?.cancel()
        subscriptions.clear()
    }
}
