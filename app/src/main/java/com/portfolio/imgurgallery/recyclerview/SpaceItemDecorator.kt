package com.portfolio.imgurgallery.recyclerview

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView

class GridSpacingItemDecoration(private val spanCount: Int, private val spacing: Int) : RecyclerView.ItemDecoration() {

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        super.getItemOffsets(outRect, view, parent, state)
        val position = parent.getChildAdapterPosition(view)
        if(position == RecyclerView.NO_POSITION) {
            return
        }
        val spanIndex = (view.layoutParams as GridLayoutManager.LayoutParams).spanIndex
        val adapter = parent.adapter ?: return
        val type = adapter.getItemViewType(position)
        when (type) {
            RecyclerViewAdapter.EMPTY_MINI_ITEM_TYPE, RecyclerViewAdapter.MINI_ITEM_TYPE -> {
                if (spanIndex == 0) {
                    outRect.left = spacing
                    outRect.right = spacing / 2
                } else {
                    outRect.left = spacing / 2
                    outRect.right = spacing
                }

                if (position < spanCount) {
                    outRect.top = spacing
                }
                outRect.bottom = spacing
            }

            RecyclerViewAdapter.EMPTY_FULL_ITEM_TYPE, RecyclerViewAdapter.FULL_ITEM_TYPE -> {
                outRect.left = spacing
                outRect.right = spacing
                outRect.bottom = spacing
            }
            }
        }
}