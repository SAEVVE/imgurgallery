package com.portfolio.imgurgallery.util

import android.animation.ValueAnimator
import android.view.View
import android.view.animation.AccelerateDecelerateInterpolator

class Animation {
    companion object {
        private const val SCALE_ANIMATION_DURATION = 200L

        fun getScaleAnimation(view: View) : ValueAnimator {
            val animator = ValueAnimator.ofFloat(1f, 1.15f, 1f)
            animator.duration = SCALE_ANIMATION_DURATION
            animator.interpolator = AccelerateDecelerateInterpolator()
            animator.addUpdateListener {
                view.scaleX = it.animatedValue as Float
                view.scaleY = it.animatedValue as Float
                view.requestLayout()
            }
            return animator
        }
    }
}