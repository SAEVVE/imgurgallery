package com.portfolio.imgurgallery.util

import android.util.Log
import androidx.room.EmptyResultSetException
import retrofit2.HttpException
import java.net.SocketTimeoutException
import java.net.UnknownHostException

private const val AUTHENTICATION_ERROR_CODE = 401
private const val ACCESS_ERROR_CODE = 403
private const val RATE_LIMIT_CODE = 429
private const val INTERNAL_SERVER_ERROR_CODE = 500

enum class ErrorType {
    SERVER_ERROR,
    INTERNET_ABSENCE_ERROR,
    RATE_LIMIT_ERROR,
    ACCESS_ERROR,
    CONNECTION_TIMEOUT_ERROR,
    DATABASE_ERROR,
    AUTHENTICATION_ERROR,
    UNKNOWN_ERROR
}

data class AppError(val errorType: ErrorType, val isRetryable: Boolean)

open class ErrorHandler {
    open fun getAppErrorByThrowable(it: Throwable): AppError {
        Log.e("Error happened", it.toString())
        var errorType = ErrorType.UNKNOWN_ERROR
        var isRetryable = true
        if (it is HttpException) {
            when (it.code()) {
                INTERNAL_SERVER_ERROR_CODE -> {
                    errorType = ErrorType.SERVER_ERROR
                }
                RATE_LIMIT_CODE -> {
                    errorType = ErrorType.RATE_LIMIT_ERROR
                    isRetryable = false
                }
                ACCESS_ERROR_CODE -> {
                    errorType = ErrorType.ACCESS_ERROR
                    isRetryable = false
                }
                AUTHENTICATION_ERROR_CODE -> {
                    errorType = ErrorType.AUTHENTICATION_ERROR
                    isRetryable = false
                }
                else -> ErrorType.UNKNOWN_ERROR
            }
        } else when (it) {
            is UnknownHostException -> {
                errorType = ErrorType.INTERNET_ABSENCE_ERROR
            }
            is SocketTimeoutException -> {
                errorType = ErrorType.CONNECTION_TIMEOUT_ERROR
            }
            is EmptyResultSetException -> {
                errorType = ErrorType.DATABASE_ERROR
            }
            else -> ErrorType.UNKNOWN_ERROR
        }
        return AppError(errorType, isRetryable)
    }
}