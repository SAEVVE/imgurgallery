package com.portfolio.imgurgallery.util.schedulers

import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

interface BaseSchedulers {
    fun io(): Scheduler
    fun ui(): Scheduler
}

class RuntimeSchedulers: BaseSchedulers {
    override fun io() = Schedulers.io()
    override fun ui() = AndroidSchedulers.mainThread()
}

open class TestSchedulers: BaseSchedulers {
    override fun io() = Schedulers.trampoline()
    override fun ui() = Schedulers.trampoline()
}