package com.portfolio.imgurgallery

import com.nhaarman.mockitokotlin2.verify
import com.portfolio.imgurgallery.gallery.GalleryPresenter
import com.portfolio.imgurgallery.gallery.GalleryRepository
import com.portfolio.imgurgallery.gallery.MessageType
import com.portfolio.imgurgallery.util.ErrorHandler
import com.portfolio.imgurgallery.util.schedulers.TestSchedulers
import io.reactivex.Completable
import io.reactivex.Single
import junit.framework.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.times
import org.mockito.MockitoAnnotations

class GalleryPresenterTest {
    private val schedulers = TestSchedulers()

    @Mock
    lateinit var galleryRepository: GalleryRepository

    @Mock
    lateinit var errorHandler: ErrorHandler

    lateinit var galleryPresenter: GalleryPresenter

    @Before
    fun init() {
        MockitoAnnotations.initMocks(this)
        galleryPresenter = GalleryPresenter(galleryRepository, schedulers, errorHandler)
        `when`(errorHandler.getAppErrorByThrowable(any())).thenReturn(TestData.testAppError)
    }

    @Test
    fun `load data success`() {
        `load data test`()
    }

    @Test
    fun `load data repository error`() {
        `load data test`(true)
    }

    private fun `load data test`(withError: Boolean = false) {
        val tagList = listOf("cats", "movie")
        val onDataLoadingSubscriber = galleryPresenter.onDataLoading().test()
        val onDataComeSubscriber = galleryPresenter.onDataCome().test()
        val onMessageSubscriber = galleryPresenter.onShowMessage().test()
        val errorOccursSubscriber = galleryPresenter.onErrorOccurs().test()
        val errorHidesSubscriber = galleryPresenter.onErrorHides().test()

        `when`(galleryRepository.loadImagesByTag(any())).thenReturn(if(withError) {
            Single.error(TestData.throwable)
        } else {
            Single.just(TestData.imgurPostListForTwoTags)
        })

        galleryPresenter.loadData(tagList)

        verify(galleryRepository).loadImagesByTag(tagList)

        assertEquals(if(withError) 1 else 0, errorOccursSubscriber.valueCount())
        if(withError) {
            errorOccursSubscriber.assertValue(TestData.testAppError)
        } else {
            errorOccursSubscriber.assertNoValues()
        }

        assertEquals(1, errorHidesSubscriber.valueCount())

        assertEquals(2, onDataLoadingSubscriber.valueCount())
        onDataLoadingSubscriber.assertValues(true, false)

        assertEquals(if (withError) 0 else 1, onMessageSubscriber.valueCount())
        if (withError) {
            onMessageSubscriber.assertNoValues()
        } else {
            onMessageSubscriber.assertValue(MessageType.DATA_UPDATED)
        }

        assertEquals(1, onDataComeSubscriber.valueCount())
        onDataComeSubscriber.assertValue(if (withError) emptyList() else TestData.imgurPostListForTwoTags)

        if (withError) { // resubscribe test
            galleryPresenter.loadData(tagList)
            assertEquals(4, onDataLoadingSubscriber.valueCount())
            onDataLoadingSubscriber.assertValues(true, false, true, false)
            verify(galleryRepository, times(2)).loadImagesByTag(tagList)
        }
    }

    @Test
    fun `delete item success test`() {
        deleteItemTest()
    }

    @Test
    fun `delete item failure test`() {
        deleteItemTest(true)
    }

    private fun deleteItemTest(withError: Boolean = false) {
        val onMessageSubscriber = galleryPresenter.onShowMessage().test()
        val onPostDeletedSubscriber = galleryPresenter.onPostDeleted().test()
        val onErrorOccursSubscriber = galleryPresenter.onErrorOccurs().test()

        `when`(galleryRepository.deletePost(any())).thenReturn(if (withError) Completable.error(TestData.throwable) else Completable.complete())
        galleryPresenter.deletePost(TestData.imgurPost1)

        verify(galleryRepository).deletePost(TestData.imgurPost1)

        assertEquals(if (withError) 1 else 0, onErrorOccursSubscriber.valueCount())
        if(withError) {
            onErrorOccursSubscriber.assertValue(TestData.testAppError)
        } else {
            onErrorOccursSubscriber.assertNoValues()
        }

        assertEquals(if (withError) 0 else 1, onMessageSubscriber.valueCount())
        if (withError) {
            onMessageSubscriber.assertNoValues()
        } else {
            onMessageSubscriber.assertValue(MessageType.ITEM_DELETED)
        }

        assertEquals(if (withError) 0 else 1, onPostDeletedSubscriber.valueCount())
        if (withError) {
            onPostDeletedSubscriber.assertNoValues()
        } else {
            onPostDeletedSubscriber.assertValue(TestData.imgurPost1)
        }
        if (withError) {
            galleryPresenter.deletePost(TestData.imgurPost2)
            assertEquals(2, onErrorOccursSubscriber.valueCount())
            verify(galleryRepository).deletePost(TestData.imgurPost1)
            verify(galleryRepository).deletePost(TestData.imgurPost2)
        }
    }
}