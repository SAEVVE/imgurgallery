package com.portfolio.imgurgallery

import com.portfolio.imgurgallery.TestData.imgurPost1
import com.portfolio.imgurgallery.TestData.imgurPostListForTwoTags
import com.portfolio.imgurgallery.TestData.imgurWholeAnswer
import com.portfolio.imgurgallery.dao.ImgurPostStorage
import com.portfolio.imgurgallery.dto.Image
import com.portfolio.imgurgallery.dto.ImgurData
import com.portfolio.imgurgallery.dto.ImgurPost
import com.portfolio.imgurgallery.dto.ImgurWholeAnswer
import com.portfolio.imgurgallery.gallery.GalleryApi
import com.portfolio.imgurgallery.gallery.GalleryRepository
import com.portfolio.imgurgallery.util.schedulers.TestSchedulers
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.internal.operators.single.SingleJust
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations

class GalleryRepositoryTest {
    @Mock
    private lateinit var storage: ImgurPostStorage

    @Mock
    private lateinit var api: GalleryApi

    @Mock
    private lateinit var galleryRepository: GalleryRepository

    private val schedulers = TestSchedulers()

    private val tags = listOf("car", "ship")
    private val throwable = Throwable("ImgurRepository test throwable")
    private val idsForTwoTags = listOf<Long>(1,2,3,1,2,3)

    @Before
    fun init() {
        MockitoAnnotations.initMocks(this)
        galleryRepository = GalleryRepository(api, storage, schedulers)
    }

    @Test
    fun `delete completed success`() {
        deleteTest()
    }

    @Test
    fun `delete completed error`() {
        deleteTest(true)
    }

    private fun deleteTest(withError: Boolean = false) {
        `when`(storage.deletePost(any())).thenReturn(if (withError) Completable.error(throwable) else Completable.complete())
        val deleteSubscriber = galleryRepository.deletePost(imgurPost1).test()

        verify(storage).deletePost(imgurPost1)

        if(withError) {
            deleteSubscriber.assertNotComplete()
            deleteSubscriber.assertError(throwable)
        } else {
            deleteSubscriber.assertComplete()
            deleteSubscriber.assertNoErrors()
        }
    }

    @Test
    fun `load data by all tags success`() {
        test_loadDataByTags()
    }

    @Test
    fun `load data by all tags api failure`() {
        test_loadDataByTags(isApiError = true)
    }

    @Test
    fun `load data by all tags storage insert failure`() {
        test_loadDataByTags(isInsertError = true)
    }

    @Test
    fun `load data by all tags storage getDataByIds failure`() {
        test_loadDataByTags(isGetDataByIdsError = true)
    }

    private fun test_loadDataByTags(isApiError: Boolean = false, isInsertError: Boolean = false, isGetDataByIdsError: Boolean = false) {
        `when`(api.getImagesByTag(any())).thenReturn(if (isApiError) Flowable.error(throwable) else (Flowable.just(imgurWholeAnswer)))
        `when`(storage.insert(any())).thenReturn(if (isInsertError) Single.error(throwable) else Single.just(idsForTwoTags))
        `when`(storage.getDataByIds(any())).thenReturn(if (isGetDataByIdsError) Single.error(throwable) else Single.just(imgurPostListForTwoTags))

        val testSubscriber = galleryRepository.loadImagesByTag(tags).test()

        verify(api).getImagesByTag(tags[0])
        verify(api).getImagesByTag(tags[1])

        if (isApiError || isInsertError || isGetDataByIdsError) {
            if (isApiError) {
                verify(storage, never()).insert(any())
                verify(storage, never()).getDataByIds(any())
            } else {
                verify(storage).insert(imgurPostListForTwoTags)
                if (isInsertError) {
                    verify(storage, never()).getDataByIds(any())
                } else {
                    verify(storage).getDataByIds(idsForTwoTags)
                }
            }
            testSubscriber.assertError(throwable)
            testSubscriber.assertNoValues()
        } else {
            testSubscriber.assertNoErrors()
            testSubscriber.assertValue(imgurPostListForTwoTags)
        }
    }
 }