package com.portfolio.imgurgallery

import com.portfolio.imgurgallery.dto.Image
import com.portfolio.imgurgallery.dto.ImgurData
import com.portfolio.imgurgallery.dto.ImgurPost
import com.portfolio.imgurgallery.dto.ImgurWholeAnswer
import com.portfolio.imgurgallery.util.AppError
import com.portfolio.imgurgallery.util.ErrorType

internal object TestData {
    val image1 = Image("http://site.com/images/1.jpg", "jpg", false)
    val image2 = Image("http://site.com/images/2.jpg", "jpg", false)
    val image3 = Image("http://site.com/images/3.jpg", "jpg", false)
    val imageList = listOf(image1, image2, image3)
    val imgurPost1 = ImgurPost(1, "id1", "title", 100, 1100, 2100, imageList, 0)
    val imgurPost2 = ImgurPost(2, "id2", "title", 200, 2100, 3100, imageList, 0)
    val imgurPost3 = ImgurPost(3, "id3", "title", 300, 3100, 4100, imageList, 0)
    val imgurPostListForOneTag = listOf(imgurPost1, imgurPost2, imgurPost3)
    val imgurPostListForTwoTags = listOf(imgurPost1, imgurPost2, imgurPost3, imgurPost1, imgurPost2, imgurPost3)
    val imgurWholeAnswer = ImgurWholeAnswer(ImgurData(imgurPostListForOneTag))
    val throwable = Throwable("ImgurRepository test throwable")
    val testAppError = AppError(ErrorType.UNKNOWN_ERROR, true)
}